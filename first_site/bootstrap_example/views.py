from django.views.generic import TemplateView


class BIndexView(TemplateView):
    template_name = "bindex.html"


bindex_view = BIndexView.as_view()
