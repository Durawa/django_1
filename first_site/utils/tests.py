from django.test import TestCase
from django.urls import reverse


class IndexViewTests(TestCase):
    def test_page_is_loaded(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, "Welcome in Django World!")
        self.assertContains(response, "Bootstrap example")
        self.assertContains(response, "About")


class AboutViewTests(TestCase):
    def test_page_is_loaded(self):
        response = self.client.get(reverse('about'))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, "Hi! I'm Damian and I work as Developer :)")
        self.assertContains(response, "Index")
